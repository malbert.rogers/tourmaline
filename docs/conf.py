extensions = ['sphinx.ext.autosectionlabel', 'sphinx_rtd_theme']

autosectionlabel_prefix_document = True

project = "Tourmaline MMB UMCU"
html_logo = "png/tourmaline_logo.png"

html_theme = 'sphinx_rtd_theme'
