.. _supportedq2options:

Description
+++++++++++

If you have used QIIME 2 before, you might be wondering which QIIME 2
commands Tourmaline uses and supports. All commands are specified as
rules in ``Snakefile``, and typical workflows without and with sequence
filtering are shown as directed acyclic graphs in the folder ``dags/``.

List of options
+++++++++++++++

The main analysis features and options supported by Tourmaline and
specified by the Snakefile are as follows:

-  FASTQ sequence import using a manifest file, or use your pre-imported
   FASTQ .qza file
-  Denoising with `DADA2 <https://doi.org/10.1038/nmeth.3869>`__
   (paired-end and single-end) and
   `Deblur <https://doi.org/10.1128/msystems.00191-16>`__ (single-end)
-  Feature classification (taxonomic assignment) with options of `naive
   Bayes <https://doi.org/10.1186/s40168-018-0470-z>`__, consensus
   `BLAST <https://doi.org/10.1186/1471-2105-10-421>`__, and consensus
   `VSEARCH <https://doi.org/10.7717/peerj.2584>`__
-  Feature filtering by taxonomy, sequence length, feature ID, and
   abundance/prevalence
-  De novo multiple sequence alignment with
   `MUSCLE <https://doi.org/10.1093/nar/gkh340>`__, `Clustal
   Omega <https://doi.org/10.1007/978-1-62703-646-7_6>`__, or
   `MAFFT <https://doi.org/10.1093/molbev/mst010>`__ (with masking) and
   tree building with
   `FastTree <https://doi.org/10.1093/molbev/msp077>`__
-  Outlier detection with
   `odseq <https://doi.org/10.1186/s12859-015-0702-1>`__
-  Interactive taxonomy barplot
-  Tree visualization using
   `Empress <https://doi.org/10.1128/mSystems.01216-20>`__
-  Alpha diversity, alpha rarefaction, and alpha group significance with
   four metrics: Faith’s phylogenetic diversity, observed features,
   Shannon diversity, and Pielou’s evenness
-  Beta diversity distances, principal coordinates,
   `Emperor <https://doi.org/10.1186/2047-217x-2-16>`__ plots, and beta
   group significance (one metadata column) with four metrics:
   unweighted and weighted
   `UniFrac <https://doi.org/10.1038/ismej.2010.133>`__, Jaccard
   distance, and Bray–Curtis distance
-  Robust Aitchison PCA and biplot ordination using
   `DEICODE <https://doi.org/10.1128/mSystems.00016-19>`__
