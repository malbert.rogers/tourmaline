.. _mergingruns:

Description
+++++++++++

For certain projects it might not be possible to run all the samples
in a single sequence run. The samples are then divided over multiple 
sequence runs. The main problem here is however, the use of the same barcodes 
in these sequencing runs and the generation of the feature tables. 
This makes it necessary for a slightly different approach to analyzing the data using Tourmaline. 
In short, for each sequence run a seperate Tourmaline will be cloned to run only the demux step of Tourmaline. 
Next, the demultiplexed_renamed fastq files from run 1 are moved and added to the folder containing the demultiplexed_renamed fastq files from run 2. In addition, the metadata files from run 1 and 2 will be combined and copied to the 00-data folder. After these steps are completed the remaining of the pipeline can be run.

Instructions
++++++++++++

For each sequence run, follow the set up in the Quick Start Guide and run (``./sbatch_tourmaline.sh demux``) 
in seperate cloned tourmaline folders:

1. Combine the fastq-files located in
   ``00-data/fastq/demultiplexed_renamed/`` in one of the run’s
   demultiplexed_renamed folder. Example:

::

   mv tourmaline_project_name_run1/00-data/fastq/demultiplexed_renamed/*.fastq.gz tourmaline_project_name_run2/00-data/fastq/demultiplexed_renamed/

*In this example, because the readfiles were moved from tourmaline_project_name_run1 to  
``tourmaline_project_name_run2``, this should then also be the folder in which to
execute further QIIME2 analyses using the Tourmaline pipeline (example
shown below at step 3).*

2. Combine the validated metadata.tsv files of the different runs and copy that to ‘00-data/metadata.tsv’ 
*In the example, copy to tourmaline_project_name_run2/00-data

3. Run the Tourmaline pipeline (samples from the different runs will be
   analyzed together). Example:

::

   ./sbatch_tourmaline.sh taxonomy
