.. _viewingoutput:

Description
+++++++++++

The Tourmaline pipeline outputs different types of file that can be viewed/inspected in different ways. Below are the recommended methods to view/inspect the .qzv, .tsv, .html and .pdf output files.

QZV 
+++

To view .qzv output (QIIME 2 visualization) files, drag and drop these in https://view.qiime2.org. Empress trees (i.e. ``rooted_tree.qzv``) may take more than 10 minutes to load.

TSV 
+++

TSV or 'Tab-separated values' files can be opened in Microsoft Excel or Tabview (command line tool that comes with Tourmaline).

HTML report
+++++++++++

If you run the Tourmaline pipeline all the way through, it should output an HTML report containing a summary of the analyses and results. Open your HTML report (i.e. ``03-reports/report_dada2-pe_unfiltered.html``) in
`Chrome <https://www.google.com/chrome/>`__ or `Firefox <https://www.mozilla.org/en-US/firefox/new/>`__

PDF 
+++

Click to open and view in new tab. Most systems should have a program already installed that is able to open PDF (portable document format)-files.
