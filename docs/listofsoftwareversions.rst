.. _listofsoftwareversions:

Tourmaline software/database versions
+++++++++++++++++++++++++++++++++++++++++++++

- **Tourmaline:** v2.3
- **QIIME 2:** qiime2-2022.2
- **Taxonomy database:** SILVA-138
- **For more information on Tourmaline software:** consult the `logs/` directory after running Tourmaline
