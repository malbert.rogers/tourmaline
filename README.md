The official documentation for this MMB-UMCU adapted Tourmaline pipeline is available on Read the Docs: https://tourmaline-mmb-umcu.readthedocs.io/


# Tourmaline

Tourmaline is an amplicon sequence processing workflow for Illumina sequence data that uses [QIIME 2](https://qiime2.org) and the software packages it wraps. Tourmaline manages commands, inputs, and outputs using the [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow management system.

For some visual overviews of the Tourmaline workflow, please, check out the DAG plots located in the `dags/` folder.

# Table of Contents

* [Quick Start Guide](#quick-start-guide)
* [Viewing Output](#viewing-output)
* [Merging Sequencing Runs](#merging-sequencing-runs)
* [More Tips](#more-tips)
* [List of Tourmaline Features](#list-of-tourmaline-features)
* [Supported QIIME 2 Options](#supported-qiime2-options)
* [More Info About Tourmaline](#more-info-about-tourmaline)

### Quick Start Guide

Below is a (4-steps) Quick Start Guide on how to run this stable version of the MMB UMCU Tourmaline version on the HPC. For this guide, it is assumed that the setup and installation were already successfully completed. If you have yet to complete the setup and installation, please do not use this Quick Start Guide and jump to 'Setup' further below. 

**Step 1:**
Clone this repository into your `data/` folder on the HPC:

```
cd data/
git clone --depth 1 --branch v2.3 https://gitlab.com/malbert.rogers/tourmaline.git tourmaline_project_name
```
*Replace 'tourmaline_project_name' with the name of your project.*

HAVING TROUBLE WITH GIT CLONE? In case git clone does not work, you could try to download the reporsitory as a zip-file using the Download button (next to the Clone button), transfer this file to the HPC and unzip.

**Step 2:**
Transfer the readfiles (R1 and R2) and the metadata (mapping/metadata file) to the cloned folder `tourmaline_project_name/` using WinSCP (for instuctions see [Transfer Files Using WinSCP](http://143.121.18.159/doku.php?id=transfer_files_using_winscp)).

**Step 3:**
Open and edit the configuration file, config.yaml:

```
nano config.yaml
```
Edit the the values for the following parameters:
* *R1_fastq_file:* forward (R1) readfile name including all extensions (example_L001_R1_001.fastq.gz).
* *R2_fastq_file:* reverse (R2) readfile name including all extensions (example_L001_R2_001.fastq.gz).
* *beta_group_column:* a column header of the metadata file. This is required for the pipeline to run. You could pick a random column header or a column header of interest, if you would also like alpha- and beta-diversity analyses performed (on this specific column). 

```
R1_fastq_file:
R2_fastq_file:
beta_group_column: 
```

In the case that you would also like Tourmaline to generate alpha- and beta-diversity outputs, it's then recommended to also adapt the values for following parameters (more information on these parameters can be found in the configuration file):

```
core_sampling_depth: 
alpha_max_depth: 
```
Save and quit nano. You do this by pressing Ctrl + X (to quit), then press Y (to save) and finally press ENTER.

**Step 4:**
Validate the metadata file and check for possible warnings/errors:
```
./validate_metadata.py metadata_file_example.txt
```

*The script will try to remove all special characters (only dots, underscores and dashes are accepted) from the metadata file, however, it might not be able to remove all. So, preferably, make sure that this file does not contain any special characters before continuing on with the next step.* 

**Step 5:**
Run Tourmaline:

For QIIME 2 analyses to obtain features-tables and taxonomy* outputs, run the following command:

```
./sbatch_tourmaline.sh taxonomy
```

*Input options for running the pipeline are: 

1. *demux*: only demultiplex the sequencing data, generating seperate fastq-files for each sample (recommended as a first step for projects' data that are split over multiple sequencing runs);  
2. *denoise*: imports FASTQ data and runs denoising, generating a feature table and representative sequences;
3. *taxonomy*: assigns taxonomy to representative sequences (recommended option to run in most cases);
4. *diversity*: step does representative sequence curation, core diversity analyses, and alpha and beta group significance;
5. *report*: step generates an HTML report of the outputs plus metadata, inputs, and parameters. Also, the *report* step can be run immediately to run the entire workflow.

--- END OF QUICK START GUIDE ---

### Viewing Output

* QZV (QIIME 2 visualization): drag and drop in [https://view.qiime2.org](https://view.qiime2.org). Empress trees (e.g., `rooted_tree.qzv`) may take more than 10 minutes to load.
* TSV (tab-separated values): open in Microsoft Excel or Tabview (command line tool that comes with Tourmaline).
* HTML report: Open your HTML report (e.g., `03-reports/report_dada2-pe_unfiltered.html`) in [Chrome](https://www.google.com/chrome/) or [Firefox](https://www.mozilla.org/en-US/firefox/new/)
* PDF (portable document format): click to open and view in new tab.

### Merging Sequencing Runs

If you would like to merge multiple sequencing runs (from the same project), after demultiplexing each sequencing run (`./sbatch_tourmaline.sh demux`) in seperate cloned tourmaline folders:

1. Gather the fastq-files located in `00-data/fastq/demultiplexed_renamed/` in one of the run's demultiplexed_renamed folder. Example:
```
mv tourmaline_project_name_run1/00-data/fastq/demultiplexed_renamed/*.fastq.gz tourmaline_project_name_run2/00-data/fastq/demultiplexed_renamed/
```
*So, in this example, because the readfiles were gathered in `tourmaline_project_name_run2`, this should be the folder in which to execute further QIIME 2 analyses using the Tourmaline pipeline (example shown below at step 3).*

2. Adapt the '00-data/metadata.tsv' (or copy a new version of the metadata.tsv to the HPC) to contain information about samples from the different runs of the projects.

3. Run the Tourmaline pipeline (samples from the different runs will be analyzed together), example:

```
./sbatch_tourmaline.sh taxonomy
```
### More Tips

* The whole workflow with test data should take ~30 minutes to complete. A normal dataset may take several hours to complete.
* If you want to re-run and not save previous output, run the following command (while inside your project folder):

```
srun --pty bash
./scripts/remove_output.sh
```
**WARNING:** This will remove ALL previously generated output. 

### List of Tourmaline software/database versions

* **Tourmaline:** v2.3
* **QIIME 2:** qiime2-2022.2
* **Taxonomy database:** SILVA-138
* **For more information on Tourmaline software:** consult the `logs/` directory after running Tourmaline

### List of Tourmaline Features

Tourmaline has several features that enhance usability and interoperability:

* **Portability.** Native support for Linux and macOS in addition to Docker containers.
* **QIIME 2.** The core commands of Tourmaline, including the [DADA2](https://benjjneb.github.io/dada2/index.html) and [Deblur](https://github.com/biocore/deblur) packages, are all commands of QIIME 2, one of the most popular amplicon sequence analysis software tools available. You can print all of the QIIME 2 and other shell commands of your workflow before or while running the workflow.
* **Snakemake.** Managing the workflow with Snakemake provides several benefits: 
  - **Configuration file** contains all parameters in one file, so you can see what your workflow is doing and make changes for a subsequent run.
  - **Directory structure** is the same for every Tourmaline run, so you always know where your outputs are.
  - **On-demand commands** mean that only the commands required for output files not yet generated are run, saving time and computation when re-running part of a workflow.
* **Parameter optimization.** The configuration file and standard directory structure make it simple to test and compare different parameter sets to optimize your workflow. Included code helps choose read truncation parameters and identify outliers in representative sequences (ASVs).
* **Visualizations and reports.** Every Tourmaline run produces an HTML report containing a summary of your metadata and outputs, with links to web-viewable QIIME 2 visualization files.
* **Downstream analysis.** Analyze the output of single or multiple Tourmaline runs programmatically, with qiime2R in R or the QIIME 2 Artifact API in Python, using the provided R and Python notebooks or your own code.

### Supported QIIME 2 Options

If you have used QIIME 2 before, you might be wondering which QIIME 2 commands Tourmaline uses and supports. All commands are specified as rules in `Snakefile`, and typical workflows without and with sequence filtering are shown as directed acyclic graphs in the folder `dags/`.  The main analysis features and options supported by Tourmaline and specified by the Snakefile are as follows:

* FASTQ sequence import using a manifest file, or use your pre-imported FASTQ .qza file
* Denoising with [DADA2](https://doi.org/10.1038/nmeth.3869) (paired-end and single-end) and [Deblur](https://doi.org/10.1128/msystems.00191-16) (single-end)
* Feature classification (taxonomic assignment) with options of [naive Bayes](https://doi.org/10.1186/s40168-018-0470-z), consensus [BLAST](https://doi.org/10.1186/1471-2105-10-421), and consensus [VSEARCH](https://doi.org/10.7717/peerj.2584)
* Feature filtering by taxonomy, sequence length, feature ID, and abundance/prevalence
* De novo multiple sequence alignment with [MUSCLE](https://doi.org/10.1093/nar/gkh340), [Clustal Omega](https://doi.org/10.1007/978-1-62703-646-7_6), or [MAFFT](https://doi.org/10.1093/molbev/mst010) (with masking) and tree building with [FastTree](https://doi.org/10.1093/molbev/msp077)
* Outlier detection with [odseq](https://doi.org/10.1186/s12859-015-0702-1)
* Interactive taxonomy barplot
* Tree visualization using [Empress](https://doi.org/10.1128/mSystems.01216-20)
* Alpha diversity, alpha rarefaction, and alpha group significance with four metrics: Faith's phylogenetic diversity, observed features, Shannon diversity, and Pielou’s evenness
* Beta diversity distances, principal coordinates, [Emperor](https://doi.org/10.1186/2047-217x-2-16) plots, and beta group significance (one metadata column) with four metrics: unweighted and weighted [UniFrac](https://doi.org/10.1038/ismej.2010.133), Jaccard distance, and Bray–Curtis distance
* Robust Aitchison PCA and biplot ordination using [DEICODE](https://doi.org/10.1128/mSystems.00016-19)

### Useful QIIME 2 Resources

* [QIIME 2 User Documentation](https://docs.qiime2.org/2022.2/)
* [QIIME 2 Forum](https://forum.qiime2.org/)
* [QIIME 2 classifiers resource](https://docs.qiime2.org/2024.2/data-resources/#taxonomy-classifiers-for-use-with-q2-feature-classifier): Official QIIME2 page on which new taxonomy databases/classifiers get published.


### More Info About Tourmaline

The Tourmaline paper is published in GigaScience:

Thompson, L. R., Anderson, S. R., Den Uyl, P. A., Patin, N. V., Lim, S. J., Sanderson, G. & Goodwin, K. D. Tourmaline: A containerized workflow for rapid and iterable amplicon sequence analysis using QIIME 2 and Snakemake. GigaScience, Volume 11, 2022, giac066, https://doi.org/10.1093/gigascience/giac066

