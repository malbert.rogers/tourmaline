#!/bin/bash

if [[ $1 == "--help" || $1 == "-h" ]] || [ $# -eq 0 ]; then
    echo -e "
QIIME2 microbiome analyses pipeline for 16S data based on Tourmaline - version 2.0 

Please, first make sure you've edited the config.yaml file accordingly and added your metadata-file, (has to be) named 'metadata.tsv' to the directory '00-data/'. 
Then choose one of these input options to run the pipeline: denoise, taxonomy, diversity, report or demux. 

1) denoise: imports FASTQ data and runs denoising (DADA2), generating a feature table and representative sequences;
2) taxonomy: step assigns taxonomy to representative sequences;
3) diversity: does representative sequence curation, core diversity analyses, and alpha and beta group significance;
4) report: generates an HTML report of the outputs plus metadata, inputs, and parameters. Also, the report step can be run immediately to run the entire workflow.

*) demux: only demultiplex the sequencing data, particularly interesting when aiming to merge multiple sequencing runs.

Usage example (when only interested in a feature table, representative sequences with assigned taxonomy):

  ./sbatch_tourmaline.sh taxonomy


*In case you would only like to demultiplex (recommended as a first step if projects' data are split over multiple sequencing runs):

  ./sbatch_tourmaline.sh demux
"
    exit 0
elif [ $# -gt 1 ]; then
    echo -e "
QIIME2 microbiome analyses pipeline for 16S data based on Tourmaline - version 2.0

INPUT ERROR: Too many arguments, only 1 allowed!

Please, first make sure you've edited the config.yaml file accordingly and added your metadata-file, (has to be) named 'metadata.tsv' to the directory '00-data'.                                     
Then choose one of these input options to run the pipeline: denoise, taxonomy, diversity, report or demux.

1) denoise: imports FASTQ data and runs denoising (DADA2), generating a feature table and representative sequences;
2) taxonomy: step assigns taxonomy to representative sequences;
3) diversity: does representative sequence curation, core diversity analyses, and alpha and beta group significance; 
4) report: generates an HTML report of the outputs plus metadata, inputs, and parameters. Also, the report step can be run immediately to run the entire workflow.

*) demux: only demultiplex the sequencing data, particularly interesting when aiming to merge multiple sequencing runs.

Usage example (when only interested in a feature table, representative sequences with assigned taxonomy):

  ./sbatch_tourmaline.sh taxonomy


*In case you would only like to demultiplex (recommended as a first step if projects' data are split over multiple sequencing runs):
  
  ./sbatch_tourmaline.sh demux
"
    exit 1
elif [[ $1 == "denoise" || $1 == "taxonomy" || $1 == "diversity" || $1 == "report" || $1 == "demux" ]]; then
    sbatch ./scripts/run_tourmaline.sh $1
else
    echo -e "
QIIME2 microbiome analyses pipeline for 16S data based on Tourmaline - version 2.0

INPUT ERROR: Unknown argument! '${1}' is not a valid argument!

Please, first make sure you've edited the config.yaml file accordingly and added your metadata-file, (has to be) named 'metadata.tsv' to the directory '00-data/'.
Then choose one of these input options to run the pipeline: denoise, taxonomy, diversity, report or demux.

1) denoise: imports FASTQ data and runs denoising (DADA2), generating a feature table and representative sequences;
2) taxonomy: step assigns taxonomy to representative sequences;
3) diversity: does representative sequence curation, core diversity analyses, and alpha and beta group significance; 
4) report: generates an HTML report of the outputs plus metadata, inputs, and parameters. Also, the report step can be run immediately to run the entire workflow.

*) demux: only demultiplex the sequencing data, particularly interesting when aiming to merge multiple sequencing runs.

Usage example (when only interested in a feature table, representative sequences with assigned taxonomy):

  ./sbatch_tourmaline.sh taxonomy


*In case you would only like to demultiplex (recommended as a first step if projects' data are split over multiple sequencing runs):

  ./sbatch_tourmaline.sh demux
"
    exit 1
fi
