import chardet
import codecs
import sys
import string
import os
import re

inputf = sys.argv[1]
meta_loc="00-data/metadata.tsv"

# Open the file and read the first few bytes
with open(inputf, "rb") as f:
    data = f.read()

# Detect the encoding of the file
encoding = chardet.detect(data)["encoding"]
print("Detected metadata file encoding: " + encoding + "\n")

# If the encoding is not UTF-8, convert it to UTF-8
if encoding not in ("utf-8","ascii"):
    print("WARNING: Metadadata file did not have the correct encoding. The file encoding has now been changed to UTF-8\n")
    # Open the file in the detected encoding
    with codecs.open(inputf, "r", encoding) as f:
        data = f.read()
    # Open a new file in UTF-8 encoding and write the data
    with codecs.open("metadata.tmp", "w", "utf-8") as f:
        f.write(data)
else:
    os.system("cp " + inputf + " metadata.tmp")

with open("metadata.tmp", "r") as f:
    contents = f.read()

# Search for special character in the metadata file
pattern = re.compile(r'[@!#$%^&*()<>?/\|{}~:]')

if pattern.search(contents):
    print("WARNING: Metadata file appears to contain special characters other than dots, underscores, and dashes!\nThis script will try to remove these, however there are certain characters that might not get removed and may lead to errors during running of the Tourmaline pipeline. So, please make sure the metadata file does NOT contain any non-alphanumeric characters, other than dots, underscores and dashes.\n")
else:
    print("Metadata file does not appear to contain any special characters other than dots, underscores or dashes. All good!\n")

# Remove the dot, underscore and dashfrom the string of special characters
special_characters = string.punctuation.replace(".", "").replace("_", "").replace("-", "")

# Make a translation table to remove all other special characters
translator = contents.maketrans('','',special_characters)

filtered_contents = contents.translate(translator)

with open(meta_loc, 'w') as file:
    file.write(filtered_contents)

# Remove temporary file
os.system("rm metadata.tmp")
