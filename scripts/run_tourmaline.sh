#!/bin/bash

#SBATCH --job-name=tourmaline
#SBATCH --mem=100G
#SBATCH --time=10:00:00
#SBATCH --cpus-per-task=16
#SBATCH -o %x-%j.o 
#SBATCH -e %x-%j.e
#SBATCH --gres=tmpspace:100G

#export TMPDIR="/hpc/tmp"

if [[ $1 == "denoise" || $1 == "taxonomy" || $1 == "diversity" || $1 == "report" ]]; then
    if [[ ! -f 01-imported/classifier.qza ]]; then 
	cp /hpc/local/Rocky8/dla_mm/tools/qiime2/silva-138-99-nb-classifier0.24.1.qza 01-imported/classifier.qza
    fi
    singularity exec -H $PWD -B $PWD:/data -B $TMPDIR:$TMPDIR -B $TMPDIR:/home/qiime2/ --pwd data /hpc/local/Rocky8/dla_mm/tools/qiime2/qiime2_tourmaline_container/tourmaline-qiime2-2022.2_latest.sif snakemake dada2_pe_${1}_unfiltered --cores 16 
    singularity exec -H $PWD -B $PWD:/data -B $TMPDIR:$TMPDIR -B $TMPDIR:/home/qiime2/ --pwd data /hpc/local/Rocky8/dla_mm/tools/qiime2/qiime2_tourmaline_container/tourmaline-qiime2-2022.2_latest.sif snakemake --unlock
    echo "QIIME2 pre-trained classifier used: silva-138-99-nb-classifier0.24.1.qza" >> logs/classifier.log
elif [[ $1 == "demux" ]]; then
    singularity exec -H $PWD -B $PWD:/data -B $TMPDIR:$TMPDIR -B $TMPDIR:/home/qiime2/ --pwd data /hpc/local/Rocky8/dla_mm/tools/qiime2/qiime2_tourmaline_container/tourmaline-qiime2-2022.2_latest.sif snakemake fastq_rename_demux --cores 16
    singularity exec -H $PWD -B $PWD:/data -B $TMPDIR:$TMPDIR -B $TMPDIR:/home/qiime2/ --pwd data /hpc/local/Rocky8/dla_mm/tools/qiime2/qiime2_tourmaline_container/tourmaline-qiime2-2022.2_latest.sif snakemake --unlock
echo -e "
If you would like to merge multiple sequencing runs (from the same project), after demultiplexing (./run_tourmaline demux): 
1) Gather the fastq-files located in '00-data/fastq/demultiplex_renamed/' in one of the run's demultiplex_renamed directory. Example:
   mv tourmaline_project_name_run1/00-data/fastq/demultiplex_renamed/*.fastq.gz tourmaline_project_name_run2/00-data/fastq/demultiplex_renamed/

So, in this example, because the readfiles were gathered in 'tourmaline_project_name_run2', this should be the directory in which to execute further QIIME2 analyses using the Tourmaline pipeline.

2) Adapt the '00-data/metadata.tsv' (or copy a new version of the metadata.tsv to the HPC) to contain information about samples from the different runs of the projects.

3) Run the Tourmaline pipeline (samples from the different runs will be analyzed together), example:
  ./run_tourmaline taxonomy 
"
else
    echo -e "
QIIME2 microbiome analyses pipeline for 16S data based on Tourmaline - version 2.0 

Please choose one of these four steps to run the pipeline: denoise, taxonomy, diversity or report. 

1) denoise: imports FASTQ data and runs denoising, generating a feature table and representative sequences;
2) taxonomy: step assigns taxonomy to representative sequences;
3) diversity: does representative sequence curation, core diversity analyses, and alpha and beta group significance;
4) report: generates an HTML report of the outputs plus metadata, inputs, and parameters. Also, the *report* step can be run immediately to run the entire workflow.

*) demux: only demultiplex the sequencing data, particularly interesting when aiming to merge multiple sequencing runs.

Usage example:

  ./run_tourmaline.sh taxonomy

*In case you would only like to demultiplex (recommended as a first step if projects' data are split over multiple sequencing runs):

  ./run_tourmaline.sh demux
"
    exit 1
fi

