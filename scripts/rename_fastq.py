#!/usr/bin/env python

import os
import csv
import sys

defaultpath= "00-data/fastq/demultiplexed_cutadapt/"
samplenr=1

with open(sys.argv[1], 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter='\t', quotechar='"')
    next(csvreader, None)
    for row in csvreader:
        samplename_R1 = defaultpath + row[0] + "_S" + str(samplenr) + '_L001_R1_001.fastq.gz'
        barcodeseq_R1 = defaultpath + row[1] + '_R1.fastq.gz'
        samplename_R2 = defaultpath + row[0] + "_S" + str(samplenr) + '_L001_R2_001.fastq.gz'
        barcodeseq_R2 = defaultpath + row[1] + '_R2.fastq.gz'
        if os.path.exists(barcodeseq_R1):
            os.rename(barcodeseq_R1, samplename_R1)
            os.system("cp " + samplename_R1 + " 00-data/fastq/demultiplexed_renamed/")
            print ("Renamed " + barcodeseq_R1 + " to " + samplename_R1)
        else:
            print (barcodeseq_R1 + " does not exist")
        if os.path.exists(barcodeseq_R2):
            os.rename(barcodeseq_R2, samplename_R2)
            print ("Renamed " + barcodeseq_R2 + " to " + samplename_R2)
            os.system("cp " + samplename_R2 + " 00-data/fastq/demultiplexed_renamed/")
        else:
            print (barcodeseq_R2 + " does not exist")
        samplenr=samplenr+1
